<?php

function set_active($path, $active = 'active') {
    return call_user_func_array('Request::is', (array)$path) ? $active : '';
}

function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}

/**
 *
 * Get the auth NTID of the user
 *
 */
function getNtid()
{
    if (env("USER") == "AUTH_USER") {
        return substr(env('AUTH_USER'), 10);
    }

    return env('TEST', false);
}
